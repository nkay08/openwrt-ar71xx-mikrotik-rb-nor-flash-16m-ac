## Flashing
- connect your computer via ethernet to LAN port 1 (WAN) of the device
- set your computers network address to `192.168.1.10`
- use TFTP and set a boot file
    - use the initramfs file (e.g. `openwrt-ar71xx-mikrotik-rb-nor-flash-16M-ac-initramfs-kernel.bin`)
- wait until the image was uploaded and flashed, this can take a moment to some minutes
- connect your computer to LAN port 2 of the device
- ping `192.168.1.1` until device can be reached
- `ssh root@192.168.1.1`
- if you see OpenWrt the image should work and you can flash the actual image
    - if it does not work even after significant time the image doesnt work on your device
        - restart the device to undo changes
- disconnect ssh and `scp <image> root@192.168.1.1:/tmp/` with a corresponding sysupgrade image
- ssh into the device again and do `sysupgrade /tmp/<image>`
- wait until the image was flashed

### Linux TFTP
Replace `<image>` and `<network_interface>`
```
USER=$(whoami)
sudo /sbin/ip addr replace 192.168.1.10/24 dev <network_interface>
sudo /sbin/ip link set dev em1 up
sudo /usr/sbin/dnsmasq --user=${USER} \
                       --no-daemon \
                       --listen-address 192.168.1.10 \
                       --bind-interfaces \
                       -p0 \
                       --dhcp-authoritative \
                       --dhcp-range=192.168.1.100,192.168.1.200 \
                       --bootp-dynamic \
                       --dhcp-boot=<image> \
                       --log-dhcp \
                       --enable-tftp \
                       --tftp-root=$(pwd)
```
### Windows TFTP
- TFTPD
- TinyPXE
- ...
- 

# Boot device  into netboot mode
- (best to take the board out of its case, so you can reach reset button easier)
- unplug power
- hold reset button
- plug in power while still holding the reset button
- keep holding reset
- one LED will start blinking and then a constant light. keep holding
- the LED will eventually turn off (after ~15secs)
- you can release the reset button now


# Building openwrt
- use `openwrt-branch.sh` on the corresponding branch
- (go into openwrt folder and checkout branch or pull updates)
- build according to [https://openwrt.org/docs/guide-developer/build-system/use-buildsystem](https://openwrt.org/docs/guide-developer/build-system/use-buildsystem)
