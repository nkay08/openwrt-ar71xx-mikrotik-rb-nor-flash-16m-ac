BASEDIR=$(dirname "$0")

if [ ! -d "$BASEDIR/openwrt" ]
then
	git clone https://git.openwrt.org/openwrt/openwrt.git
fi

if [ ! -f "$BASEDIR/openwrt/Makefile" ]
then
	git init openwrt
	cd "$BASEDIR/openwrt"
	rm "$BASEDIR/openwrt/.gitignore"
	git remote add origin https://git.openwrt.org/openwrt/openwrt.git
	git pull origin master
fi

if [ -f "$BASEDIR/openwrt-branch" ]
then
    BRANCH=`cat ${BASEDIR}/openwrt-branch`
    echo "$BRANCH"
    if [ ! -z "$BRANCH" ]
    then
		echo "checking out openwrt-branch ${BRANCH}"
		cd "$BASEDIR/openwrt"
		git pull
		git checkout "$BRANCH"
		git pull origin "$BRANCH"
	else
		echo "openwrt-branch is empty"
		cd "$BASEDIR/openwrt"
		git  pull
		git  checkout master
		git pull origin master
    fi
fi
